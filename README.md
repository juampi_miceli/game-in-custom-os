# Game in custom OS

This is a project for the subject Computer Organization II. It consists in coding a basic OS that will run a simple game. This project is perfect for learning these concepts: Segmentation, Pagination, Interrupts, Protection and Scheduling.