/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================

  Definicion de funciones del scheduler
*/

#include "sched.h"
#include "defines.h"
#include "mmu.h"
#include "tss.h"
#include "i386.h"
#include "screen.h"
#include "game.h"
#include "prng.h"

uint8_t tareas_vivas_rick[PLAYER_TASKS];
uint8_t tareas_vivas_morty[PLAYER_TASKS];
uint16_t tareas_rick[PLAYER_TASKS];
uint16_t tareas_morty[PLAYER_TASKS];
uint32_t meeseeks_time_left[TOTAL_MEESEEKS];
uint8_t used_portal[TOTAL_MEESEEKS];
uint32_t rick_cr3;
uint32_t morty_cr3;


//0 = rick
//1 = morty
uint8_t last_player = MORTY;
uint8_t last_rick_task = -1;
uint8_t last_morty_task = -1;



void sched_init(void) {

	tareas_rick[0] = RICK_TASK_SEL;
	tareas_rick[1] = TASK_1_SEL;
	tareas_rick[2] = TASK_2_SEL;
	tareas_rick[3] = TASK_3_SEL;
	tareas_rick[4] = TASK_4_SEL;
	tareas_rick[5] = TASK_5_SEL;
	tareas_rick[6] = TASK_6_SEL;
	tareas_rick[7] = TASK_7_SEL;
	tareas_rick[8] = TASK_8_SEL;
	tareas_rick[9] = TASK_9_SEL;
	tareas_rick[10] = TASK_10_SEL;

	tareas_morty[0] = MORTY_TASK_SEL;
	tareas_morty[1] = TASK_11_SEL;
	tareas_morty[2] = TASK_12_SEL;
	tareas_morty[3] = TASK_13_SEL;
	tareas_morty[4] = TASK_14_SEL;
	tareas_morty[5] = TASK_15_SEL;
	tareas_morty[6] = TASK_16_SEL;
	tareas_morty[7] = TASK_17_SEL;
	tareas_morty[8] = TASK_18_SEL;
	tareas_morty[9] = TASK_19_SEL;
	tareas_morty[10] = TASK_20_SEL;


	for (int i = 0; i < PLAYER_TASKS; ++i)
	{
		tareas_vivas_rick[i] = 0;
		tareas_vivas_morty[i] = 0;
	}
	tareas_vivas_rick[0] = 1;
	tareas_vivas_morty[0] = 1;

	for (int i = 0; i < TOTAL_MEESEEKS; ++i)
	{
		meeseeks_time_left[i] = 16;
		used_portal[i] = 0;

	}

}

uint8_t exist_meeseeks(uint8_t *tareas_vivas){
	for (int i = 1; i < PLAYER_TASKS; ++i)
	{
		if(tareas_vivas[i] == 1) return 1; 
	}
	return 0;
}


int8_t get_random_meeseeks(uint8_t *tareas_vivas){
	if(exist_meeseeks(tareas_vivas) == 1){
		uint32_t index = 100;
		do{
		index = rand();
		index %= PLAYER_TASKS;

		}while(index == 0 || tareas_vivas[index] == 0);

		return index;
	}

	return -1;
}
// paddr_t get_phy(uint32_t cr3, vaddr_t virt);
// vaddr_t get_virt(uint16_t task_selector);
uint32_t get_meeseeks_pos(uint16_t task_selector, uint32_t cr3){
	vaddr_t virt = get_virt(task_selector);
	// uint32_t cr3 = rcr3();
	paddr_t phy  = get_phy(cr3, virt);  

	return physical_2_pos(phy);
}

void use_portal(uint16_t task_selector){
	uint8_t indice_tarea = get_indice_tarea(task_selector);

	int8_t random_meeseeks_index = -1;
	uint32_t new_position = rand();
	new_position %= TOTAL_CELLS;
	uint32_t current_position = -1;
	uint32_t current_x_pos = -1;
	uint32_t current_y_pos = -1;

	uint32_t new_x_pos = get_x_coord(new_position);
	uint32_t new_y_pos = get_y_coord(new_position);
	uint16_t new_task_selector = -1;


	if(indice_tarea <= 10){
		//Es una tarea de rick
		random_meeseeks_index = get_random_meeseeks(&tareas_vivas_morty[0]);
		if(random_meeseeks_index == -1) return;
		lcr3(get_morty_cr3());

		new_task_selector = tareas_morty[random_meeseeks_index];

	}else{
		//Es una tarea de morty
		
		random_meeseeks_index = get_random_meeseeks(&tareas_vivas_rick[0]);
		if(random_meeseeks_index == -1) return;
		lcr3(get_rick_cr3());
		new_task_selector = tareas_rick[random_meeseeks_index];
	}

	//breakpoint();
	current_position = get_meeseeks_pos(new_task_selector, rcr3());
	current_x_pos = get_x_coord(current_position);
	current_y_pos = get_y_coord(current_position);

	int32_t delta_x = new_x_pos - current_x_pos;
	int32_t delta_y = new_y_pos - current_y_pos;

	move_meeseeks_portal(delta_x, delta_y, new_task_selector);


	if(indice_tarea <= 10){
		lcr3(get_rick_cr3());
		used_portal[indice_tarea-1] = 1;
	}else{

		lcr3(get_morty_cr3());
		used_portal[indice_tarea-2] = 1;
	}

}

uint8_t has_used_portal(uint16_t task_selector){

	uint8_t indice_tarea = get_indice_tarea(task_selector);

	if(indice_tarea <= 10){
		//Es una tarea de rick
		return used_portal[indice_tarea-1];

	}else{
		//Es una tarea de morty
		return used_portal[indice_tarea-2];
	}
}

void reduce_movement(uint16_t task_selector){
	if(task_selector == RICK_TASK_SEL || task_selector == MORTY_TASK_SEL){
		return;
	}

	uint8_t indice_tarea = get_indice_tarea(task_selector);

	if(indice_tarea <= 10){
		//Es una tarea de rick
		if(meeseeks_time_left[indice_tarea-1] > 2){
			meeseeks_time_left[indice_tarea-1]--;
		}

	}else{
		//Es una tarea de morty
		if(meeseeks_time_left[indice_tarea-2] > 2){
			meeseeks_time_left[indice_tarea-2]--;
		}
	}
}

uint32_t get_max_distance(uint16_t task_selector){
	uint8_t indice_tarea = get_indice_tarea(task_selector);

	if(indice_tarea <= 10){
		//Es una tarea de rick
		return meeseeks_time_left[indice_tarea-1]/2;

	}else{
		//Es una tarea de morty
		return meeseeks_time_left[indice_tarea-2]/2;
	}
	return -1;
}

uint16_t sched_next_task(void) {
	int abortar = 0;
	if(last_player == RICK){
		last_morty_task++;

		while(tareas_vivas_morty[last_morty_task] == 0){
			abortar++;
			if(last_morty_task < 9){
				last_morty_task++;
			}else{
				last_morty_task = 0;
			}
			if(abortar > 50){
				return -1;
			}
		}

		last_player = MORTY;
		return tareas_morty[last_morty_task];

	}else{
		last_rick_task++;

		while(tareas_vivas_rick[last_rick_task] == 0){
			abortar++;
			if(last_rick_task < 9){
				last_rick_task++;
			}else{
				last_rick_task = 0;
			}
			if(abortar > 50){
				return -1;
			}
		}
		last_player = RICK;
		return tareas_rick[last_rick_task];
	}
}


// ;void desalojar_tarea(sel_task){
// ;       indice = conseguir_indice_tareas()
// ;       vir = conseguir_virtual_dir()
// ;       unmap(vir)
// ;       jugador = conseguir_jugador()
// ;       tareas_vivas_jugador[indice] = 0
// ;       limpiar_pila()
// ;}

uint8_t get_indice_tarea(uint16_t task_selector){
	for (int i = 0; i < PLAYER_TASKS; ++i)
	{
		if(tareas_rick[i] == task_selector){
			return i;
		}
		if(tareas_morty[i] == task_selector){
			return i+PLAYER_TASKS;
		}
	}

	return -1;
}

paddr_t desalojar_tarea(uint16_t task_selector){
	uint8_t indice_tarea = get_indice_tarea(task_selector);
	vaddr_t virtual_dir = get_virt(task_selector);
	uint32_t cr3 = rcr3();

	if(indice_tarea <= 10){
		//Es una tarea de rick

		tareas_vivas_rick[indice_tarea] = 0;
		
	}else{
		//Es una tarea de morty
		indice_tarea -= PLAYER_TASKS;
		tareas_vivas_morty[indice_tarea] = 0;
	}
	paddr_t phy = -1;
	if(virtual_dir == TASK_CODE_VIRTUAL){
		phy = mmu_unmap_pages(cr3, virtual_dir, 4);
	}else{
		phy = mmu_unmap_pages(cr3, virtual_dir, 2);
	}

	return phy;
}

paddr_t sacar_tarea(uint16_t task_selector){

	uint8_t indice_tarea = get_indice_tarea(task_selector);
	vaddr_t virtual_dir = get_virt(task_selector);
	uint32_t cr3 = rcr3();

	paddr_t phy = get_phy(cr3, virtual_dir);

	if(indice_tarea <= 10){
		//Es una tarea de rick
		tareas_vivas_rick[indice_tarea] = 0;
		
	}else{
		//Es una tarea de morty
		indice_tarea -= PLAYER_TASKS;
		tareas_vivas_morty[indice_tarea] = 0;
	}


	mmu_map_pages(cr3, phy, phy, MMU_P | MMU_W | MMU_U , 2);
	mmu_unmap_pages(cr3, virtual_dir, 2);

	return phy;
}

int8_t mover_tarea(uint32_t x, uint32_t y, paddr_t old_phy, uint16_t task_selector){
	uint32_t cr3 = rcr3();
	uint32_t indice_meeseeks = get_indice_tarea(task_selector);
	vaddr_t virtual_dir = get_virt(task_selector);
	

	//Chequear si hay una semilla en (x,y)

	int8_t seed_index = check_for_seed(x, y);

	if(seed_index != -1){
		destruir_semilla(seed_index);
		//Sumar puntos al que corresponda
		uint32_t jugador_actual;
		if(indice_meeseeks <= 10){
			//Es una tarea de rick
			jugador_actual = RICK;
		}else{
			//Es una tarea de morty
			jugador_actual = MORTY;
		}
		sumar_puntaje(jugador_actual);
		mmu_unmap_pages(cr3, old_phy, 2);
		return 0;
	}

	if(indice_meeseeks <= 10){
		//Es una tarea de rick
		tareas_vivas_rick[indice_meeseeks] = 1;

	}else{
		//Es una tarea de morty
		indice_meeseeks -= PLAYER_TASKS;
		tareas_vivas_morty[indice_meeseeks] = 1;
	}
	uint32_t phy = get_physical_dir(x, y);
	mmu_map_pages(cr3, virtual_dir, phy, MMU_P | MMU_W | MMU_U ,2);

	code_copy(old_phy, virtual_dir, MEESEEKS_SIZE);
	mmu_unmap_pages(cr3, old_phy, 2);

	return 1;
}


uint32_t alojar_tarea(uint32_t code_start, uint32_t x, uint32_t y){
	uint16_t task_selector = rtr();
	int16_t new_meeseeks_sel = -1;
	uint32_t cr3 = rcr3();


	if(task_selector == RICK_TASK_SEL){
		//Es un meeseeks de rick
		new_meeseeks_sel = get_meeseeks_sel(RICK);
	}else if(task_selector == MORTY_TASK_SEL){
		//Es un meeseeks de morty
		new_meeseeks_sel = get_meeseeks_sel(MORTY); 
	}else{
		//Hay un error. Fuerzo error 14.
		int8_t *puntero = 0x0000;
  		puntero[0] = 42;
	}

	//Chequeo si hay espacio para otra tarea
	if(new_meeseeks_sel == -1){
		return 0;
	}


	//Chequear si hay una semilla en (x,y)

	int8_t seed_index = check_for_seed(x, y);

	if(seed_index != -1){
		destruir_semilla(seed_index);
		//Sumar puntos al que corresponda
		uint32_t jugador_actual;
		if(task_selector == RICK_TASK_SEL){
			jugador_actual = RICK;
		}else{
			jugador_actual = MORTY;
		}
		sumar_puntaje(jugador_actual);
		return 0;
	}

	uint8_t indice_meeseeks = get_indice_tarea(new_meeseeks_sel);
	uint8_t indice_tss_meeseeks;

	if(indice_meeseeks <= 10){
		//Es una tarea de rick
		tareas_vivas_rick[indice_meeseeks] = 1;
		indice_tss_meeseeks = indice_meeseeks - 1;

	}else{
		//Es una tarea de morty
		indice_tss_meeseeks = indice_meeseeks - 2;
		indice_meeseeks -= PLAYER_TASKS;
		tareas_vivas_morty[indice_meeseeks] = 1;
	}

	meeseeks_time_left[indice_tss_meeseeks] = 16;
	used_portal[indice_tss_meeseeks] = 0;
	uint32_t virt = reiniciar_tss(indice_tss_meeseeks);
	uint32_t phy = get_physical_dir(x, y);
	mmu_map_pages(cr3, virt, phy, MMU_P | MMU_W | MMU_U ,2);

	code_copy(code_start, virt, MEESEEKS_SIZE);
	return virt;
}


uint32_t physical_2_pos(paddr_t phy){
	phy -= MAP_PHY_START;
	phy /= MEESEEKS_SIZE;

	return phy;
}

uint32_t get_physical_dir(uint32_t x, uint32_t y){
	uint32_t phy = MAP_PHY_START;

	phy += (x + (y*SIZE_M)) * MEESEEKS_SIZE;
	return phy;
}


uint8_t get_meeseeks_index(uint8_t jugador){
	for (uint8_t i = 0; i < PLAYER_TASKS; ++i)
	{	
		if(jugador == RICK){
			if(tareas_vivas_rick[i] == 0){
				return i;
			}	
		}else{
			if(tareas_vivas_morty[i] == 0){
				return i;
			}

		}
		
	}
	return -1;
}

uint16_t get_meeseeks_sel(uint8_t jugador){
	int8_t indice_meeseeks = get_meeseeks_index(jugador);

	if(indice_meeseeks == -1){
		//No hay espacio libre
		return -1;
	}
	if(jugador == RICK){
		return tareas_rick[indice_meeseeks];
	}else{
		return tareas_morty[indice_meeseeks];
	}
	return -1;
}

uint8_t esta_vivo(uint8_t jugador){
	switch(jugador){
		case RICK:
			if(tareas_vivas_rick[0] == 1) return 1;
			else return 0;
		break;
		case MORTY:
			if(tareas_vivas_morty[0] == 1) return 1;
			else return 0;
		break;
		default:
			return 0;
	}
}


uint16_t get_task_selector(uint32_t jugador, uint32_t index){
	switch(jugador){
		case RICK:
			return tareas_rick[index+1];
		break;
		case MORTY:
			return tareas_morty[index+1];
		break;
		default:
		return 9999;
	}
}

uint8_t meeseeks_alive(uint32_t jugador, uint32_t index){
	switch(jugador){
		case RICK:
			return tareas_vivas_rick[index+1];
		break;
		case MORTY:
			return tareas_vivas_morty[index+1];
		break;
		default:
		return 0;
	}	
}

//void print(const char* text, uint32_t x, uint32_t y, uint16_t attr);
void imprimir_relojes(uint8_t clock){
	for (int i = 0; i < PLAYER_TASKS; ++i)
	{
		if(tareas_vivas_rick[i] == 1){
			switch(clock){
			case 0:
				print("|", 19 + i*4, 42, 0x07);
			break;
			case 1:
				print("/", 19+ i*4, 42, 0x07);
			break;
			case 2:
				print("-", 19+ i*4, 42, 0x07);
			break;
			case 3:
				print("\\", 19+ i*4, 42, 0x07);
			break;
			}
		}else{
			print("x", 19+ i*4, 42, 0x07);
		}


		if(tareas_vivas_morty[i] == 1){
			switch(clock){
			case 0:
				print("|", 19 + i*4, 47, 0x07);
			break;
			case 1:
				print("/", 19+ i*4, 47, 0x07);
			break;
			case 2:
				print("-", 19+ i*4, 47, 0x07);
			break;
			case 3:
				print("\\", 19+ i*4, 47, 0x07);
			break;
			}
		}else{
			print("x", 19+ i*4, 47, 0x07);
		}
	}
}

