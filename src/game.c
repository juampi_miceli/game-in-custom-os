/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================
*/

#include "types.h"
#include "game.h"
#include "prng.h"
#include "defines.h"
#include "screen.h"
#include "i386.h"
#include "sched.h"
#include "mmu.h"
#include "tss.h"
#include "idt.h"

int32_t seed_positions[N_SEEDS];
uint32_t rick_points = 0;
uint32_t morty_points = 0;

uint32_t tienen_codigo[20];


uint8_t duplicate_seed(uint32_t pos){
	for (uint32_t i = 0; i < N_SEEDS; ++i)
	{
		if((uint32_t)seed_positions[i] == pos){
			return 1;
		}
	}
	return 0;
}

uint32_t get_pos(uint32_t x, uint32_t y){
	return SIZE_M*y+x;
}

uint32_t get_x_coord(uint32_t pos){
	return pos%SIZE_M;
}

uint32_t get_y_coord(uint32_t pos){
	return pos/SIZE_M;
}

void place_seeds(){

	for (uint32_t i = 0; i < N_SEEDS; ++i)
	{
		seed_positions[i] = -1;
	}

	uint32_t placed_seeds = 0;
	char *semilla = "x";
	uint32_t new_seed_pos;
	uint32_t semilla_dinamica = 0;
	while(placed_seeds < N_SEEDS){
		semilla_dinamica += 83;
		srand(semilla_dinamica);
		new_seed_pos = rand();
		new_seed_pos %= TOTAL_CELLS;

		if(duplicate_seed(new_seed_pos) == 0){
			//Elegi una posicion valida
			seed_positions[placed_seeds] = new_seed_pos;
			print(semilla, get_x_coord(new_seed_pos), get_y_coord(new_seed_pos), 0xAE);
			placed_seeds++;
		}
	}

}


int8_t check_for_seed(uint32_t x,uint32_t y){
	uint32_t pos = get_pos(x, y);
	for (uint32_t i = 0; i < N_SEEDS; ++i)
	{
		if((uint32_t)seed_positions[i] == pos){
			return i;
		}
	}
	return -1;
}

void sumar_puntaje(uint32_t jugador){
	if(jugador == RICK){
		rick_points += 425;
	}else if (jugador == MORTY){
		morty_points += 425;
	}
}

void destruir_semilla(uint32_t seed_index){
	seed_positions[seed_index] = -1;
}


void print_scores(void){
	screen_draw_box(42,5,3,9,0,0x47);
	print_dec(rick_points, 7, 6, 43, 0x47);
	screen_draw_box(46,5,3,9,0,0x17);
	print_dec(morty_points, 7, 6, 47, 0x17);
}


uint32_t move_x(uint32_t pos, int32_t d_x){
	int32_t x = get_x_coord(pos);
	int32_t y = get_y_coord(pos);

	if((x + d_x) < 0){
		x = (x+d_x)+SIZE_M;
	}else{
		x = (x+d_x)%SIZE_M;
	}

	return get_pos(x, y);
}

uint32_t move_y(uint32_t pos, int32_t d_y){
	int32_t x = get_x_coord(pos);
	int32_t y = get_y_coord(pos);

	if((y + d_y) < 0){
		y = (y+d_y)+SIZE_N;
	}else{
		y = (y+d_y)%SIZE_N;
	}

	return get_pos(x, y);
	
}

uint8_t move_meeseeks_portal(int32_t d_x, int32_t d_y, uint16_t task_selector){

	paddr_t phy = sacar_tarea(task_selector);
	uint32_t pos = physical_2_pos(phy);

	pos = move_x(pos, d_x);
	pos = move_y(pos, d_y);


	mover_tarea(get_x_coord(pos), get_y_coord(pos), phy, task_selector);

	return 1;

}

uint8_t move_meeseeks(int32_t d_x, int32_t d_y, uint16_t task_selector){

	// breakpoint();
	uint32_t in_distance = abs(d_x) + abs(d_y);
	uint32_t max_distance = get_max_distance(task_selector); 

	if(max_distance < in_distance){
		return 0;
	}

	paddr_t phy = sacar_tarea(task_selector);
	uint32_t pos = physical_2_pos(phy);

	pos = move_x(pos, d_x);
	pos = move_y(pos, d_y);


	mover_tarea(get_x_coord(pos), get_y_coord(pos), phy, task_selector);

	return 1;

}

uint32_t abs(int32_t x){
	if(x<0){
		return -x;
	}else{
		return x;
	}
}

uint32_t dist_manhattan(uint32_t pos1, uint32_t pos2){
	uint32_t x1 = get_x_coord(pos1);
	uint32_t y1 = get_y_coord(pos1);
	uint32_t x2 = get_x_coord(pos2);
	uint32_t y2 = get_y_coord(pos2);

	return abs(y1-y2)+abs(x1-x2);

}

void meeseeks_look(uint32_t dir_x, uint32_t dir_y){
	uint16_t task_selector = rtr();
	uint32_t cr3 = rcr3();
	uint32_t indice_meeseeks = get_indice_tarea(task_selector);
	uint32_t virt = MEESEEKS_CODE_START + MEESEEKS_SIZE*((indice_meeseeks%PLAYER_TASKS)-1);

	paddr_t phy = get_phy(cr3, virt);
	uint32_t current_pos = physical_2_pos(phy);

	uint32_t min_distance = 500;
	uint32_t winner_pos = 2;
	uint32_t current_distance = 0;

	for(uint32_t i = 0; i < N_SEEDS; i++){
		if(seed_positions[i] != 1){
			current_distance = dist_manhattan(current_pos, seed_positions[i]);
			if(current_distance < min_distance){
				min_distance = current_distance;
				winner_pos = seed_positions[i];
			}
		}
	}

	int8_t delta_x = get_x_coord(winner_pos) - get_x_coord(current_pos);
	int8_t delta_y = get_y_coord(winner_pos) - get_y_coord(current_pos);


	int8_t *p_x = (int8_t *)dir_x;
	int8_t *p_y = (int8_t *)dir_y;
	p_x[0] = delta_x;
	p_y[0] = delta_y;
}

void portal_gun(void){
	uint16_t task_selector = rtr();
	if(has_used_portal(task_selector) == 0){
		use_portal(task_selector);
	}
}

uint8_t no_more_seeds(void){
	for (int i = 0; i < N_SEEDS; ++i)
	{
		if(seed_positions[i] != -1){
			return 0;
		}
	}

	return 1;
}
void rick_wins(void){
	screen_draw_box(0, 0, 40, 80, 0, 0x47);
	print("  _  _  _  _              _  _  _              _  _  _           _           _  ", 0, 11, 0x47);
	print(" (_)(_)(_)(_) _          (_)(_)(_)          _ (_)(_)(_) _       (_)       _ (_) ", 0, 12, 0x47);
	print(" (_)         (_)            (_)            (_)         (_)      (_)    _ (_)    ", 0, 13, 0x47);
	print(" (_) _  _  _ (_)            (_)            (_)                  (_) _ (_)       ", 0, 14, 0x47);
	print(" (_)(_)(_)(_)               (_)            (_)                  (_)(_) _        ", 0, 15, 0x47);
	print(" (_)   (_) _                (_)            (_)          _       (_)   (_) _     ", 0, 16, 0x47);
	print(" (_)      (_) _           _ (_) _          (_) _  _  _ (_)      (_)      (_) _  ", 0, 17, 0x47);
	print(" (_)         (_)         (_)(_)(_)            (_)(_)(_)         (_)         (_) ", 0, 18, 0x47);
	print("                                                                                ", 0, 19, 0x47);
	print("                                                                                ", 0, 20, 0x47);
	print(" _             _          _  _  _           _           _          _  _  _  _   ", 0, 21, 0x47);
	print("(_)           (_)        (_)(_)(_)         (_) _       (_)       _(_)(_)(_)(_)_ ", 0, 22, 0x47);
	print("(_)           (_)           (_)            (_)(_)_     (_)      (_)          (_)", 0, 23, 0x47);
	print("(_)     _     (_)           (_)            (_)  (_)_   (_)      (_)_  _  _  _   ", 0, 24, 0x47);
	print("(_)   _(_)_   (_)           (_)            (_)    (_)_ (_)        (_)(_)(_)(_)_ ", 0, 25, 0x47);
	print("(_)  (_) (_)  (_)           (_)            (_)      (_)(_)       _           (_)", 0, 26, 0x47);
	print("(_)_(_)   (_)_(_)         _ (_) _          (_)         (_)      (_)_  _  _  _(_)", 0, 27, 0x47);
	print("  (_)       (_)          (_)(_)(_)         (_)         (_)        (_)(_)(_)(_)  ", 0, 28, 0x47);                                                                               

	while(true){

	}
}

void morty_wins(void){
	screen_draw_box(0, 0, 40, 80, 0, 0x17);
	print(" _           _     _  _  _  _     _  _  _  _      _  _  _  _  _   _           _ ", 0, 12, 0x17);
	print("(_) _     _ (_)  _(_)(_)(_)(_)_  (_)(_)(_)(_) _  (_)(_)(_)(_)(_) (_)_       _(_)", 0, 13, 0x17);
	print("(_)(_)   (_)(_) (_)          (_) (_)         (_)       (_)         (_)_   _(_)  ", 0, 14, 0x17);
	print("(_) (_)_(_) (_) (_)          (_) (_) _  _  _ (_)       (_)           (_)_(_)    ", 0, 15, 0x17);
	print("(_)   (_)   (_) (_)          (_) (_)(_)(_)(_)          (_)             (_)      ", 0, 16, 0x17);
	print("(_)         (_) (_)          (_) (_)   (_) _           (_)             (_)      ", 0, 17, 0x17);
	print("(_)         (_) (_)_  _  _  _(_) (_)      (_) _        (_)             (_)      ", 0, 18, 0x17);
	print("(_)         (_)   (_)(_)(_)(_)   (_)         (_)       (_)             (_)      ", 0, 19, 0x17);
	print("                                                                                ", 0, 20, 0x17);
	print(" _             _          _  _  _           _           _          _  _  _  _   ", 0, 21, 0x17);
	print("(_)           (_)        (_)(_)(_)         (_) _       (_)       _(_)(_)(_)(_)_ ", 0, 22, 0x17);
	print("(_)           (_)           (_)            (_)(_)_     (_)      (_)          (_)", 0, 23, 0x17);
	print("(_)     _     (_)           (_)            (_)  (_)_   (_)      (_)_  _  _  _   ", 0, 24, 0x17);
	print("(_)   _(_)_   (_)           (_)            (_)    (_)_ (_)        (_)(_)(_)(_)_ ", 0, 25, 0x17);
	print("(_)  (_) (_)  (_)           (_)            (_)      (_)(_)       _           (_)", 0, 26, 0x17);
	print("(_)_(_)   (_)_(_)         _ (_) _          (_)         (_)      (_)_  _  _  _(_)", 0, 27, 0x17);
	print("  (_)       (_)          (_)(_)(_)         (_)         (_)        (_)(_)(_)(_)  ", 0, 28, 0x17); 
	while(true){

	}
}


void tie(void){
	screen_draw_box(0, 0, 50, 80, 0, 0x00);
	print(" _  _  _  _  _           _  _  _           _  _  _  _  _        _  _  _  _    ", 0, 12, 0x67);
	print("(_)(_)(_)(_)(_)         (_)(_)(_)         (_)(_)(_)(_)(_)      (_)(_)(_)(_)   ", 0, 13, 0x67);
	print("      (_)                  (_)            (_)                   (_)      (_)_ ", 0, 14, 0x67);
	print("      (_)                  (_)            (_) _  _              (_)        (_)", 0, 15, 0x67);
	print("      (_)                  (_)            (_)(_)(_)             (_)        (_)", 0, 16, 0x67);
	print("      (_)                  (_)            (_)                   (_)       _(_)", 0, 17, 0x67);
	print("      (_)                _ (_) _          (_) _  _  _  _        (_)_  _  (_)  ", 0, 18, 0x67);
	print("      (_)               (_)(_)(_)         (_)(_)(_)(_)(_)      (_)(_)(_)(_)   ", 0, 19, 0x67);
	print("                                                                              ", 0, 20, 0x67);
	print("    _  _  _                 _              _           _        _  _  _  _  _ ", 0, 21, 0x67);
	print(" _ (_)(_)(_) _            _(_)_           (_) _     _ (_)      (_)(_)(_)(_)(_)", 0, 22, 0x67);
	print("(_)         (_)         _(_) (_)_         (_)(_)   (_)(_)      (_)            ", 0, 23, 0x67);
	print("(_)    _  _  _        _(_)     (_)_       (_) (_)_(_) (_)      (_) _  _       ", 0, 24, 0x67);
	print("(_)   (_)(_)(_)      (_) _  _  _ (_)      (_)   (_)   (_)      (_)(_)(_)      ", 0, 25, 0x67);
	print("(_)         (_)      (_)(_)(_)(_)(_)      (_)         (_)      (_)            ", 0, 26, 0x67);
	print("(_) _  _  _ (_)      (_)         (_)      (_)         (_)      (_) _  _  _  _ ", 0, 27, 0x67);
	print("   (_)(_)(_)(_)      (_)         (_)      (_)         (_)      (_)(_)(_)(_)(_)", 0, 28, 0x67);

	while(true){

	}
}

void check_winner(void){
	if(no_more_seeds() == 1){
		if(rick_points > morty_points){
			rick_wins();
		}else if(rick_points < morty_points){
			morty_wins();
		}else{
			tie();
		}
	}else{
		if (esta_vivo(RICK) && !esta_vivo(MORTY))
		{
			rick_wins();
		}else if(!esta_vivo(RICK) && esta_vivo(MORTY)){
			morty_wins();
		}else if(!esta_vivo(RICK) && !esta_vivo(MORTY)){
			tie();
		}
	}
}


void imprimir_debugger(uint32_t *p_stack){
	// breakpoint();
	uint8_t i = 0;
	uint16_t gs = (uint16_t)p_stack[i++];
	uint16_t fs = (uint16_t)p_stack[i++];
	uint16_t es = (uint16_t)p_stack[i++];
	uint16_t ds = (uint16_t)p_stack[i++];
	uint32_t int_number = (uint32_t)p_stack[i++];
	uint32_t cr4 = (uint32_t)p_stack[i++];
	uint32_t cr3 = (uint32_t)p_stack[i++];
	uint32_t cr2 = (uint32_t)p_stack[i++];
	uint32_t cr0 = (uint32_t)p_stack[i++];
	uint16_t task_selector = (uint16_t)p_stack[i++];
	uint32_t edi = (uint32_t)p_stack[i++];
	uint32_t esi = (uint32_t)p_stack[i++];
	uint32_t ebp = (uint32_t)p_stack[i++];
	i++;
	uint32_t ebx = (uint32_t)p_stack[i++];
	uint32_t edx = (uint32_t)p_stack[i++];
	uint32_t ecx = (uint32_t)p_stack[i++];
	uint32_t eax = (uint32_t)p_stack[i++];
	uint16_t stack_error = 0xDEAD;
	if(tienen_codigo[int_number] == 1){
		stack_error = (uint16_t)p_stack[i++];
	}
	uint32_t eip = (uint32_t)p_stack[i++];
	uint16_t cs = (uint16_t)p_stack[i++];
	uint32_t eflags = (uint32_t)p_stack[i++];
	uint32_t esp = (uint32_t)p_stack[i++];
	uint16_t ss = (uint16_t)p_stack[i++];


	
	uint32_t stack1;
	uint32_t stack2;
	uint32_t stack3;
	uint32_t *p_esp = (uint32_t *)esp;

	vaddr_t task_mem_start = get_virt(task_selector);
	vaddr_t task_mem_end = task_mem_start + MEESEEKS_SIZE;

	// breakpoint();
	// uint32_t modulo_stack = esp%PAGE_SIZE;
	if(task_mem_end == esp){
		stack1 = 0xDEADBEEF;
		stack2 = 0xDEADBEEF;
		stack3 = 0xDEADBEEF;
	}else if(task_mem_end == esp + 4){
		stack1 = p_esp[0];
		stack2 = 0xDEADBEEF;
		stack3 = 0xDEADBEEF;
	}else if(task_mem_end == esp + 8){
		stack1 = p_esp[0];
		stack2 = p_esp[1];
		stack3 = 0xDEADBEEF;
	}else{
		stack1 = p_esp[0];
		stack2 = p_esp[1];
		stack3 = p_esp[2];
	}



	//Cuadro negro
	screen_draw_box(0, 0, 40, 80, 0, 0x07);

	//Numero de interrupcion
	print_int(int_number, 20, 5);

	//Indice de tarea
	uint8_t index = get_indice_tarea(task_selector);

	if(index == 0){
		print("R", 60, 5, 0x04);
	}else if(index == 11){
		print("M", 60, 5, 0x01);
	}else if(index <= 10){
		print_dec(index, 3,60, 5,0x04);
	}else{
		print_dec(index-PLAYER_TASKS, 3,60, 5,0x01);
	}


	i = 10;
	print("eax", 15,i++,0x07);
	print("ebx", 15,i++,0x07);
	print("ecx", 15,i++,0x07);
	print("edx", 15,i++,0x07);
	print("esi", 15,i++,0x07);
	print("edi", 15,i++,0x07);
	print("ebp", 15,i++,0x07);
	print("esp", 15,i++,0x07);
	print("eip", 15,i++,0x07);
	print("cs", 16,i++,0x07);
	print("ds", 16,i++,0x07);
	print("es", 16,i++,0x07);
	print("fs", 16,i++,0x07);
	print("gs", 16,i++,0x07);
	print("ss", 16,i++,0x07);
	i++;
	print("eflags", 15,i++,0x07);

	i = 10;

	print_hex(eax, 8, 19, i++, 0x02);
	print_hex(ebx, 8, 19, i++, 0x02);
	print_hex(ecx, 8, 19, i++, 0x02);
	print_hex(edx, 8, 19, i++, 0x02);
	print_hex(esi, 8, 19, i++, 0x02);
	print_hex(edi, 8, 19, i++, 0x02);
	print_hex(ebp, 8, 19, i++, 0x02);
	print_hex(esp, 8, 19, i++, 0x02);
	print_hex(eip, 8, 19, i++, 0x02);
	print_hex(cs, 8, 19, i++, 0x02);
	print_hex(ds, 8, 19, i++, 0x02);
	print_hex(es, 8, 19, i++, 0x02);
	print_hex(fs, 8, 19, i++, 0x02);
	print_hex(gs, 8, 19, i++, 0x02);
	print_hex(ss, 8, 19, i++, 0x02);
	i++;
	print_hex(eflags, 8, 22, i++, 0x02);
	// print("int_number", 0,i++,0x07);

	i = 10;

	print("cr0", 30,i++,0x07);
	print("cr2", 30,i++,0x07);
	print("cr3", 30,i++,0x07);
	print("cr4", 30,i++,0x07);
	if(tienen_codigo[int_number] == 1){
		print("err", 30,i++,0x07);
	}
	i++;
	print("stack", 30,i++,0x07);

	i = 10;

	print_hex(cr0, 8, 34, i++, 0x02);
	print_hex(cr2, 8, 34, i++, 0x02);
	print_hex(cr3, 8, 34, i++, 0x02);
	print_hex(cr4, 8, 34, i++, 0x02);

	if(tienen_codigo[int_number] == 1){
		print_hex(stack_error, 8, 34, i++, 0x02);
	}
	i+=2;
	print_hex(stack1, 8, 30, i++, 0x02);
	print_hex(stack2, 8, 30, i++, 0x02);
	print_hex(stack3, 8, 30, i++, 0x02);

	i = 10;
	print("backtrace", 45, i++, 0x07);
	// breakpoint();
	uint32_t *p_ebp = (uint32_t *)ebp; 
	while((uint32_t)p_ebp >= task_mem_start && (uint32_t)p_ebp < task_mem_end - 4){
		uint32_t iret = p_ebp[1];
		print_hex(iret, 8, 45, i++, 0x02);
		p_ebp = (uint32_t *)p_ebp[0];
	}



	



}


//tienen_codigo nos dice si la excepcion i del procesador tiene codigo de error o no.
void game_init(void){

	for (int i = 0; i < 20; ++i)
	{
		tienen_codigo[i] = 0;
	}
	tienen_codigo[8] = 1;
	tienen_codigo[10] = 1;
	tienen_codigo[11] = 1;
	tienen_codigo[12] = 1;
	tienen_codigo[13] = 1;
	tienen_codigo[14] = 1;
	tienen_codigo[17] = 1;
}


void actualizar_pantalla(){
	int32_t seed_pos = -1;
	int32_t rick_meeseeks_pos = -1;
	int32_t morty_meeseeks_pos = -1;
	uint8_t vivo = 0;


	uint8_t *buffer = (uint8_t *)(0x23000);
	uint8_t *screen = (uint8_t *)VIDEO;

	
	for (int i = 0; i < SIZE_M*SIZE_N; ++i)
	{	
		print_2_buffer(buffer, " ", get_x_coord(i), get_y_coord(i), 0x25);
	}

	for (uint32_t i = 0; i < N_SEEDS; ++i)
	{
		seed_pos = seed_positions[i];
		if(seed_pos != -1){
			print_2_buffer(buffer, "X", get_x_coord(seed_pos), get_y_coord(seed_pos), 0x2E);
			
		}
	}

	for(uint32_t i = 0; i < TOTAL_MEESEEKS/2; i++){
		vivo = meeseeks_alive(RICK, i);
		if(vivo == 1){
			uint16_t task_selector = get_task_selector(RICK, i);
			rick_meeseeks_pos = get_meeseeks_pos(task_selector, get_rick_cr3());
			print_2_buffer(buffer, "R", get_x_coord(rick_meeseeks_pos), get_y_coord(rick_meeseeks_pos), 0x24);
		}
	}

	for(uint32_t i = 0; i < TOTAL_MEESEEKS/2; i++){
		vivo = meeseeks_alive(MORTY, i);
		if(vivo == 1){
			uint16_t task_selector = get_task_selector(MORTY, i);
			morty_meeseeks_pos = get_meeseeks_pos(task_selector, get_morty_cr3());
			print_2_buffer(buffer, "M", get_x_coord(morty_meeseeks_pos), get_y_coord(morty_meeseeks_pos), 0x21);
		}
	} 

	for (int i = 0; i < SIZE_M*SIZE_N*2; ++i)
	{
		screen[i] = buffer[i];
	}
}