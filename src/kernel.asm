; ** por compatibilidad se omiten tildes **
; ==============================================================================
; TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
; ==============================================================================

%include "print.mac"
%define  CS_RING_0_SEL  0x0050
%define  DS_RING_0_SEL  0x0058
%define  SCREEN_MEM_SEL 0x0070
%define INIT_TASK_SEL   0x0078
%define IDLE_TASK_SEL   0x0080

%define TASK_1           0x0013
;Informacion GDT
extern GDT_DESC

extern pic_reset
extern pic_enable

;Informacion IDT
extern idt_init
extern IDT_DESC

;Informacion MMU
extern mmu_init
extern mmu_init_kernel_dir
extern mmu_init_task_dir
extern mmu_map_page
extern mmu_unmap_page
%define MMU_P (1 << 0)
%define MMU_W (1 << 1)
%define MMU_U (1 << 2)



;seeds
extern place_seeds

;TSS
extern tss_init


;SCHED
extern sched_init

;Game 
extern game_init

global start

BITS 16
;; Saltear seccion de datos
jmp start

;;
;; Seccion de datos.
;; -------------------------------------------------------------------------- ;;
start_rm_msg db     'Iniciando kernel en Modo Real'
start_rm_len equ    $ - start_rm_msg

start_pm_msg db     'Iniciando kernel en Modo Protegido'
start_pm_len equ    $ - start_pm_msg

pintar_linea_msg  times 80 db ' '
pintar_linea_len  equ    $ - pintar_linea_msg

info_meeseeks_R_msg db  'R   00  01  02  03  04  05  06  07  08  09'
info_meeseeks_R_len equ $ - info_meeseeks_R_msg

info_meeseeks_M_msg db  'M   00  01  02  03  04  05  06  07  08  09'
info_meeseeks_M_len equ $ - info_meeseeks_M_msg

borde_cuadro_msg times 9 db  ' '
borde_cuadro_len equ $ - borde_cuadro_msg

lib_1_msg db '000/19'
lib_1_len equ $ - lib_1_msg
lib_2_msg db '001/19'
lib_2_len equ $ - lib_2_msg
lib_3_msg db '002/19'
lib_3_len equ $ - lib_3_msg

fondo_mapa:   db     0x22 

tss_offset: dd 0x0000
tss_segsel: dw 0x00


;;
;; Seccion de código.
;; -------------------------------------------------------------------------- ;;

BITS 32




pintar_mapa_fs:
    push    eax
    push    ebx
    push    ecx
    

    mov     ecx, 6400               ;ecx = bytes a llenar - 1
    mov 	ebx, 0					;ebx = contador
    mov     al, [fondo_mapa]        ;al  = byte color del mapa

    .ciclo_mapa:
        mov     byte [fs:ebx], 0    ;Seteo el caracter nulo
        inc     ebx                 ;Avanzo al color
        mov     [fs:ebx], al        ;Pinto la casilla
        inc     ebx                 ;Avanza a proxima casilla
        cmp 	ecx, ebx			;Chequeo si termine
        jnz    .ciclo_mapa

    pop     ecx 
    pop     ebx
    pop     eax
    ret




pintar_score_bars:
    push    ebx
    push    ecx

    mov     ecx, info_meeseeks_R_len    ;Guardo el largo del msj en eax (son los 2 igual de largos)
    mov     ebx, 80                     ;Guardo el largo de la fila en r8d
    sub     ebx, ecx                    ;Ahora tengo el espacio vacio en la fila despues del mensaje
    shr     ebx, 1                      ;Divido por 2 para conseguir la columna de inicio del msj

    print_text_pm info_meeseeks_R_msg, info_meeseeks_R_len, 0x04, 41, ebx
    print_text_pm info_meeseeks_M_msg, info_meeseeks_M_len, 0x01, 45, ebx

    pop     ecx
    pop     ebx
    ret







;; Punto de entrada del kernel.
BITS 16
start:
    ; Deshabilitar interrupciones
    cli

    ; Cambiar modo de video a 80 X 50
    mov ax, 0003h
    int 10h ; set mode 03h
    xor bx, bx
    mov ax, 1112h
    int 10h ; load 8x8 font

    ; Imprimir mensaje de bienvenida
    print_text_rm start_rm_msg, start_rm_len, 0x07, 0, 0

    ;xchg bx, bx

    ; Habilitar A20
    call A20_disable
    call A20_check
    call A20_enable
    call A20_check

    ; Cargar la GDT

    lgdt [GDT_DESC]

    ; Setear el bit PE del registro CR0

    mov eax, cr0
    or  eax, 0x1
    mov cr0, eax
    
    ; Saltar a modo protegido
    jmp     CS_RING_0_SEL:modo_protegido

BITS 32
modo_protegido:
    ; Establecer selectores de segmentos

    mov ax, DS_RING_0_SEL
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov ss, ax

    ; Establecer la base de la pila
    
    mov ebp, 0x25000
    mov esp, 0x25000

    ;xchg bx, bx

    ; Imprimir mensaje de bienvenida
    print_text_pm pintar_linea_msg, pintar_linea_len, 0x00, 1, 0
    print_text_pm pintar_linea_msg, pintar_linea_len, 0x00, 2, 0
    print_text_pm pintar_linea_msg, pintar_linea_len, 0x00, 3, 0
    print_text_pm pintar_linea_msg, pintar_linea_len, 0x00, 4, 0
    print_text_pm start_pm_msg, start_pm_len, 0x07, 0, 0

    ; xchg bx,bx

    ;xchg bx, bx

    ; Inicializar pantalla
    mov ax, SCREEN_MEM_SEL
    mov fs, ax
    call pintar_mapa_fs

    call pintar_score_bars
    ; xchg bx, bx



    ; Inicializar semillas
    ; xchg bx, bx
    call place_seeds

    ; Inicializar el manejador de memoria
    call mmu_init

    ;xchg bx, bx

    ; Inicializar el directorio de paginas
    call  mmu_init_kernel_dir

    ;xchg bx,bx
    
    ; Cargar directorio de paginas
    mov eax, cr3
    or  eax, 0x25000
    mov cr3, eax

    ; Habilitar paginacion
    mov eax, cr0
    or  eax, 0x80000000
    mov cr0, eax

    ; Imprimir Libretas Luego de Paginacion
    

    ; xchg bx,bx

    ; Inicializar tss (idle, init task, rick, morty y meeseeks)
    
    call tss_init

    ; Inicializar el scheduler

    call sched_init


    ; xchg bx, bx

    ; Inicializar la IDT
    call idt_init   
    ;xchg bx,bx

    ; Cargar IDT
    lidt [IDT_DESC] ;Cargar IDT

    
    ;xchg bx, bx

    ; Configurar controlador de interrupciones
    call pic_reset
    call pic_enable
    

    ; Cargar tarea inicial

    mov ax, INIT_TASK_SEL
    ltr ax

    call game_init

    ; Habilitar interrupciones
    sti

    ; Saltar a la primera tarea: Idle
    xchg bx, bx
    jmp IDLE_TASK_SEL:0x0

    ; Ciclar infinitamente (por si algo sale mal...)
    mov eax, 0xFFFF
    mov ebx, 0xFFFF
    mov ecx, 0xFFFF
    mov edx, 0xFFFF
    jmp $

;; -------------------------------------------------------------------------- ;;

%include "a20.asm"





;Esto de abajo no se usa en el codigo final

    ; ; void mmu_map_page(
    ; ;                   uint32_t cr3: Cuarto Push
    ; ;                   vaddr_t virt: Tercer Push
    ; ;                   paddr_t phy: Segundo Push
    ; ;                   uint32_t attrs: Primer push
    ; ;)

    ; push MMU_P | MMU_W          ;Permiso de escritura
    ; push 0x00400000             ;Direccion fisica 
    ; push 0x0050E000             ;Direccion virtual

    ; mov eax, cr3
    ; push eax                    ;CR3

    ; xchg bx,bx

    ; call mmu_map_page           ;Los parametros se pasan por pila.

    ; xchg bx,bx
    ; add esp, 4*4

    ; mov [0x0050E300], eax

    ; ; paddr_t mmu_unmap_page(uint32_t cr3, vaddr_t virt) {}
    ; push 0x0050E000
    ; mov eax, cr3
    ; push eax
    ; call mmu_unmap_page
    ; add esp, 2*4



    ; ; paddr_t mmu_init_task_dir(paddr_t phy_start, paddr_t code_start, size_t pages)
    ; xchg bx, bx

    ; mov eax, cr3
    ; push eax

    ; push 4
    ; push 0x14000
    ; push 0x1D04000
    ; call mmu_init_task_dir
    ; add esp, 4*3
    ; mov cr3, eax

    ; mov byte [fs:0x001], 0x00
    ; mov byte [fs:0x000], 0x00



    ; pop eax
    ; mov cr3, eax