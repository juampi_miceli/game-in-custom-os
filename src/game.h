/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================

  Declaracion de las rutinas asociadas al juego.
*/

#ifndef __GAME_H__
#define __GAME_H__


typedef enum e_task_type {
  Rick = 1,
  Morty = 2,
  Meeseeks = 3,
} task_type_t;



uint8_t duplicate_seed(uint32_t pos);
uint32_t get_pos(uint32_t x, uint32_t y);
uint32_t get_x_coord(uint32_t pos);
uint32_t get_y_coord(uint32_t pos);
int8_t check_for_seed(uint32_t x,uint32_t y);
void place_seeds();
void sumar_puntaje(uint32_t jugador);
void destruir_semilla(uint32_t seed_index);
void print_scores(void);
void meeseeks_look(uint32_t dir_x, uint32_t dir_y);
uint8_t move_meeseeks(int32_t d_x, int32_t d_y, uint16_t task_selector);
uint8_t move_meeseeks_portal(int32_t d_x, int32_t d_y, uint16_t task_selector);
uint32_t move_x(uint32_t pos, int32_t d_x);
uint32_t move_y(uint32_t pos, int32_t d_y);
uint32_t abs(int32_t x);
uint32_t dist_manhattan(uint32_t pos1, uint32_t pos2);
void portal_gun(void);
void check_winner(void);
uint8_t no_more_seeds(void);
void rick_wins(void);
void morty_wins(void);
void tie(void);
void imprimir_debugger(uint32_t *p_stack);
void game_init(void);
void actualizar_pantalla(void);






#endif //  __GAME_H__
