
/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================
  definicion de las rutinas de atencion de interrupciones
*/

#include "idt.h"
#include "defines.h"
#include "isr.h"
#include "screen.h"

idt_entry_t idt[255] = {0};

idt_descriptor_t IDT_DESC = {sizeof(idt) - 1, (uint32_t)&idt};



/*
    La siguiente es una macro de EJEMPLO para ayudar a armar entradas de
    interrupciones. Para usar, descomentar y completar CORRECTAMENTE los
    atributos y el registro de segmento. Invocarla desde idt_inicializar() de
    la siguiene manera:

    void idt_inicializar() {
        IDT_ENTRY(0);
        ...
        IDT_ENTRY(19);
        ...
    }
*/


#define IDT_ENTRY(numero) 																			\
    idt[numero].offset_15_0 = (uint16_t) ((uint32_t)(&_isr ## numero) & (uint32_t) 0xFFFF); 		\
    idt[numero].segsel = (uint16_t) CS_RING_0_SEL; 														\
    idt[numero].attr = (uint16_t) INTERRUPT_0_ATTRS; 												\
    idt[numero].offset_31_16 = (uint16_t) ((uint32_t)(&_isr ## numero) >> 16 & (uint32_t) 0xFFFF);

#define IDT_ENTRY_SYSCALLS(numero)																	\
	idt[numero].offset_15_0 = (uint16_t) ((uint32_t)(&_isr ## numero) & (uint32_t) 0xFFFF); 		\
    idt[numero].segsel = (uint16_t) CS_RING_0_SEL; 														\
    idt[numero].attr = (uint16_t) INTERRUPT_3_SYSCALLS; 											\
    idt[numero].offset_31_16 = (uint16_t) ((uint32_t)(&_isr ## numero) >> 16 & (uint32_t) 0xFFFF);


void idt_init() {
    IDT_ENTRY(0);
    IDT_ENTRY(1);
    IDT_ENTRY(2);
    IDT_ENTRY(3);
    IDT_ENTRY(4);
    IDT_ENTRY(5);
    IDT_ENTRY(6);
    IDT_ENTRY(7);
    IDT_ENTRY(8);
    IDT_ENTRY(9);
    IDT_ENTRY(10);
    IDT_ENTRY(11);
    IDT_ENTRY(12);
    IDT_ENTRY(13);
    IDT_ENTRY(14);
    IDT_ENTRY(16);
    IDT_ENTRY(17);
    IDT_ENTRY(18);
    IDT_ENTRY(19);
    IDT_ENTRY(32);
    IDT_ENTRY(33);
    IDT_ENTRY_SYSCALLS(88);
    IDT_ENTRY_SYSCALLS(89);
    IDT_ENTRY_SYSCALLS(100);
    IDT_ENTRY_SYSCALLS(123);
    
}



void print_int(int numero, int x, int y){
	switch (numero){
		case 0: 
			print("#DE Error de division. [0]", x,y, 0x02);
		break;
		case 1:
			print("#DB Error reservado. [1]", x, y, 0x02);
		break;
		case 2:
			print("Interrupcion NMI. [2]", x, y, 0x02);
		break;
		case 3:
			print("#BP BreakPoint. [3]", x, y, 0x02);
		break;
		case 4:
			print("#OF Error de Overflow. [4]", x, y, 0x02);
		break;
		case 5:
			print("#BR Error: Rango limite excedido. [5]", x, y, 0x02);
		break;
		case 6:
			print("#UD Codigo de operacion invalida. [6]", x, y, 0x02);
		break;
		case 7:
			print("#NM Coprocesador no disponible. [7]", x, y, 0x02);
		break;
		case 8:
			print("#DF Falla doble. [8]", x, y, 0x02);
		break;
		case 9:
			print("Segmento de coprocesador excedido. [9]", x, y, 0x02);
		break;
		case 10:
			print("#TS TSS invalido. [10]", x, y, 0x02);
		break;
		case 11:
			print("#NP Segmento no presente. [11]", x, y, 0x02);
		break;
		case 12:
			print("#SS Falla en el segmento de stack. [12]", x, y, 0x02);
		break;
		case 13:
			print("#GP Error de proteccion general. [13]", x, y, 0x02);
		break;
		case 14:
			print("#PF Error de pagina. [14]", x, y, 0x02);
		break;
		case 16:
			print("#MF Error en la FPU. [16]", x, y, 0x02);
		break;
		case 17:
			print("#AC Error de alineacion. [17]", x, y, 0x02);
		break;
		case 18:
			print("#MC Machine check. [18]", x, y, 0x02);
		break;		
		case 19:
			print("#XF Excepcion del punto flotante en SIMD. [19]", x, y, 0x02);
		break;
	}
	
}