/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================

  Definicion de funciones del manejador de memoria
*/

#include "mmu.h"
#include "i386.h"
#include "sched.h"
#include "kassert.h"


#define PAGE_DIRECTORY_ENTRY_NULL 0x00000000
#define PAGE_TABLE_ENTRY_NULL 0x00000000



uint32_t proxima_pagina_libre;

void mmu_init(void) {
  proxima_pagina_libre = INICIO_DE_PAGINAS_LIBRES;     
}

paddr_t mmu_next_free_kernel_page(void) {
  uint32_t pagina_libre = proxima_pagina_libre;
  proxima_pagina_libre += PAGE_SIZE;
  return pagina_libre;
}


paddr_t mmu_init_kernel_dir(void) {

  page_directory_entry *pd = (page_directory_entry*) KERNEL_PAGE_DIR;
  page_table_entry *pt_0 = (page_table_entry*) KERNEL_PAGE_TABLE_0; 

  //Inicializacion en 0
  for (int i = 0; i < 1024; i++){
  	pd[i] = (page_directory_entry){0};
  	pt_0[i] = (page_table_entry){0};
  }

  pd[0].present = 1;
  pd[0].read_write = 1;
  pd[0].user_supervisor = 0;
  pd[0].page_table_base = ((uint32_t)pt_0)>>12; 

  for (int i = 0; i < 1024; i++){
  	pt_0[i].present = 1;
  	pt_0[i].read_write = 1;
  	pt_0[i].user_supervisor = 0;
  	pt_0[i].physical_address_base = i; 	//Por ser identity maping
  }



  return (uint32_t) pd;
}



void mmu_map_page(uint32_t cr3, vaddr_t virt, paddr_t phy, uint32_t attrs) {

  page_directory_entry *pd = (page_directory_entry *) (cr3 & ~0xFFF);  
  vaddr_t directory_index = virt >> 22;
  uint32_t table_index = (virt >> 12) & 0x3FF;
  if (pd[directory_index].present != 1){
    page_table_entry *new_pt = (page_table_entry *) mmu_next_free_kernel_page();
    //Inicializo en 0 la nueva page Table.
    for (int i = 0; i < 1024; i++){
     new_pt[i] = (page_table_entry){0};
    }
    pd[directory_index].page_table_base = (uint32_t)(new_pt) >>12;
    pd[directory_index].present = attrs & 0x1;
    pd[directory_index].read_write = (attrs >> 1) & 0x1;
    pd[directory_index].user_supervisor = (attrs >> 2) & 0x1;
  }
  page_table_entry *pt = (page_table_entry *)(pd[directory_index].page_table_base << 12); 
  pt[table_index].physical_address_base = phy >>12;
  pt[table_index].present = attrs & 0x1;
  pt[table_index].read_write = (attrs >> 1) & 0x1;
  pt[table_index].user_supervisor = (attrs >> 2) & 0x1;
  

  tlbflush();
}

void mmu_map_pages(uint32_t cr3, vaddr_t virt, paddr_t phy, uint32_t attrs, size_t pages){

  for (size_t i = 0; i < pages; ++i)
  {
    mmu_map_page(cr3, virt + i*PAGE_SIZE, phy + i*PAGE_SIZE, attrs);
  }
}


paddr_t get_phy(uint32_t cr3, vaddr_t virt){
  page_directory_entry *pd = (page_directory_entry *) cr3;  
  uint32_t directory_index = virt >> 22;
  uint32_t table_index = (virt >> 12) & 0x3FF;
  paddr_t phy = 0x00000000;
  if (pd[directory_index].present != 0){
    page_table_entry *pt = (page_table_entry *)(pd[directory_index].page_table_base << 12);
    phy = (pt[table_index].physical_address_base) << 12;
  }
  tlbflush();
  return phy;
}

vaddr_t get_virt(uint16_t task_selector){
  uint32_t indice_meeseeks = get_indice_tarea(task_selector);
  uint32_t virt = MEESEEKS_CODE_START + MEESEEKS_SIZE*((indice_meeseeks%PLAYER_TASKS)-1);

  return virt;
}

paddr_t mmu_unmap_page(uint32_t cr3, vaddr_t virt) { 

  page_directory_entry *pd = (page_directory_entry *) (cr3 & ~0xFFF);  
  uint32_t directory_index = virt >> 22;
  uint32_t table_index = (virt >> 12) & 0x3FF;
  paddr_t r = 0x00000000;
  if (pd[directory_index].present != 0){
    page_table_entry *pt = (page_table_entry *)(pd[directory_index].page_table_base << 12);
    r = (pt[table_index].physical_address_base) << 12;
    pt[table_index] = (page_table_entry){0};
  }
  tlbflush();
  return r;
}

paddr_t mmu_unmap_pages(uint32_t cr3, vaddr_t virt, size_t pages){
  paddr_t phy = -1;
  paddr_t aux_phy;
  for (uint32_t i = 0; i < pages; ++i)
  {
    aux_phy = mmu_unmap_page(cr3, virt + i*PAGE_SIZE);
    if(i == 0){
      phy = aux_phy;
    }
  }

  return phy;
}


//void mmu_map_page(uint32_t cr3, vaddr_t virt, paddr_t phy, uint32_t attrs)
paddr_t mmu_init_task_dir(paddr_t phy_start, paddr_t code_start, size_t pages) {

  uint32_t cr3_actual  = rcr3();
  mmu_map_pages(cr3_actual, TASK_CODE_VIRTUAL, phy_start, MMU_P | MMU_W, pages);
  page_directory_entry *task_pd = (page_directory_entry *)mmu_next_free_kernel_page(); 

  // breakpoint();
	//Inicializacion en 0
	for (int i = 0; i < 1024; i++){
		task_pd[i] = (page_directory_entry){0};
	}
  mmu_map_pages((uint32_t)(task_pd), 0, 0, MMU_P | MMU_W, 1024);
  mmu_map_pages((uint32_t)(task_pd), TASK_CODE_VIRTUAL, phy_start, MMU_P | MMU_W | MMU_U, pages);
  code_copy(code_start, TASK_CODE_VIRTUAL, PAGE_SIZE*TASK_PAGES);
  mmu_unmap_pages(cr3_actual, TASK_CODE_VIRTUAL, pages);
	return (uint32_t)task_pd;
}

void code_copy(uint32_t src, uint32_t dst, uint32_t bytes){
  uint8_t *p_src = (uint8_t *)src; 
  uint8_t *p_dst = (uint8_t *)dst;
  for (uint32_t i = 0; i < bytes; ++i)
  {
    p_dst[i] = p_src[i];
  }

}