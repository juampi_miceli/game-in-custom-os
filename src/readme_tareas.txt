La estrategia que usamos es la de tener 2 tipos de meeseeks:
1) Los buscadores
2) Los cazadores

Los buscadores son depositados en un lugar random del mapa, y lo primero que hacen es mirar por una semilla. Si la semilla esta lo suficientemente cerca como para llegar en un turno, la agarran la próxima vez que son llamadas. Si no, le pasan la posición de la semilla al jugador y se autodestruyen.

Los cazadores cumplen el único objetivo de ser depositados en lugares en donde existen semillas. Si son depositados en algún lugar donde no haya una semilla se van a autodestruir ni bien tengan oportunidad. (No soportaron haber fracasado pobres) 

Asique basicamente el jugador va alternando entre estos meeseeks, haciendo pruebas vimos que lo más óptimo era alternar entre 2 buscadores y 2 buscadores hasta que termine la partida. Asique idealmente el jugador no debería tener nunca más de 4 meeseeks en cancha.