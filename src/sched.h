/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================

  Declaracion de funciones del scheduler.
*/

#ifndef __SCHED_H__
#define __SCHED_H__

#include "types.h"

void sched_init();
uint16_t sched_next_task();
uint8_t get_indice_tarea(uint16_t task_selector);
uint32_t alojar_tarea(uint32_t code_start, uint32_t x, uint32_t y);
int8_t mover_tarea(uint32_t x, uint32_t y, paddr_t old_phy, uint16_t task_selector);
paddr_t sacar_tarea(uint16_t task_selector);
paddr_t desalojar_tarea(uint16_t task_selector);
uint16_t get_meeseeks_sel(uint8_t jugador);
uint8_t get_meeseeks_index(uint8_t jugador);
uint32_t get_physical_dir(uint32_t x, uint32_t y);
uint32_t physical_2_pos(paddr_t phy);
uint32_t get_meeseeks_pos(uint16_t task_selector, uint32_t cr3);

void reduce_movement(uint16_t task_selector);
uint32_t get_max_distance(uint16_t task_selector);
uint8_t has_used_portal(uint16_t task_selector);
void use_portal(uint16_t task_selector);
int8_t get_random_meeseeks(uint8_t *tareas_vivas);
uint8_t exist_meeseeks(uint8_t *tareas_vivas);

uint8_t esta_vivo(uint8_t jugador);

uint16_t get_task_selector(uint32_t jugador, uint32_t index);
uint8_t meeseeks_alive(uint32_t jugador, uint32_t index);

void imprimir_relojes(uint8_t clock);


#endif //  __SCHED_H__
