/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================

  Definiciones globales del sistema.
*/

#ifndef __DEFINES_H__
#define __DEFINES_H__
/* MMU */
/* -------------------------------------------------------------------------- */

#define MMU_P (1 << 0)
#define MMU_W (1 << 1)
#define MMU_U (1 << 2)

#define INICIO_DE_PAGINAS_LIBRES 0x100000

#define PAGE_SIZE 4096

#define MEESEEKS_SIZE 2 * PAGE_SIZE

/* Misc */
/* -------------------------------------------------------------------------- */
// Y Filas
#define SIZE_N 40

// X Columnas
#define SIZE_M 80

#define TOTAL_CELLS 	SIZE_M*SIZE_N

#define TOTAL_MEESEEKS 	20


#define RICK 0
#define MORTY 1

#define PLAYER_TASKS 11

#define N_SEEDS 40
/* Atributos IDT */
/* -------------------------------------------------------------------------- */
#define INTERRUPT_0_ATTRS 0x8E00;
#define INTERRUPT_3_SYSCALLS 0xEE00;

/* Indices en la gdt */
/* -------------------------------------------------------------------------- */
#define GDT_IDX_NULL 0

#define CS_RING_0       0x000A
#define DS_RING_0       0x000B
#define CS_RING_3       0x000C
#define DS_RING_3       0x000D
#define SCREEN_MEM_0    0x000E
#define INIT_TASK       0x000F
#define IDLE_TASK 		0x0010

#define RICK_TASK	0x0011
#define MORTY_TASK	0x0012




#define TASK_1  0x0013
#define TASK_2  0x0014
#define TASK_3  0x0015
#define TASK_4  0x0016
#define TASK_5  0x0017
#define TASK_6  0x0018
#define TASK_7  0x0019
#define TASK_8  0x001A
#define TASK_9  0x001B
#define TASK_10 0x001C
#define TASK_11 0x001D
#define TASK_12 0x001E
#define TASK_13 0x001F
#define TASK_14 0x0020
#define TASK_15 0x0021
#define TASK_16 0x0022
#define TASK_17 0x0023
#define TASK_18 0x0024
#define TASK_19 0x0025 
#define TASK_20 0x0026
#define DEBUG_TASK 0x0027


#define GDT_COUNT         40


/* Offsets en la gdt */
/* -------------------------------------------------------------------------- */

//Cambiar por selector
#define GDT_OFF_NULL_SEL 		(GDT_IDX_NULL << 3)
#define CS_RING_0_SEL         	(CS_RING_0 << 3)
#define DS_RING_0_SEL         	(DS_RING_0 << 3)
#define CS_RING_3_SEL         	(CS_RING_3 << 3) + 3
#define DS_RING_3_SEL         	(DS_RING_3 << 3) + 3 
#define SCREEN_MEM_0_SEL      	(SCREEN_MEM_0 << 3)
#define INIT_TASK_SEL         	(INIT_TASK << 3)
#define IDLE_TASK_SEL         	(IDLE_TASK << 3)

#define RICK_TASK_SEL			(RICK_TASK << 3)
#define MORTY_TASK_SEL			(MORTY_TASK << 3)

#define TASK_1_SEL 			(TASK_1 << 3) 
#define TASK_2_SEL 			(TASK_2 << 3) 
#define TASK_3_SEL 			(TASK_3 << 3) 
#define TASK_4_SEL 			(TASK_4 << 3) 
#define TASK_5_SEL 			(TASK_5 << 3) 
#define TASK_6_SEL 			(TASK_6 << 3) 
#define TASK_7_SEL 			(TASK_7 << 3) 
#define TASK_8_SEL 			(TASK_8 << 3) 
#define TASK_9_SEL 			(TASK_9 << 3) 
#define TASK_10_SEL 		(TASK_10 << 3)
#define TASK_11_SEL 		(TASK_11 << 3)
#define TASK_12_SEL 		(TASK_12 << 3)
#define TASK_13_SEL 		(TASK_13 << 3)
#define TASK_14_SEL 		(TASK_14 << 3)
#define TASK_15_SEL 		(TASK_15 << 3)
#define TASK_16_SEL 		(TASK_16 << 3)
#define TASK_17_SEL 		(TASK_17 << 3)
#define TASK_18_SEL 		(TASK_18 << 3)
#define TASK_19_SEL 		(TASK_19 << 3) 
#define TASK_20_SEL 		(TASK_20 << 3)	
#define DEBUG_TASK_SEL      (DEBUG_TASK << 3)

/* Direcciones de memoria */
/* -------------------------------------------------------------------------- */

// direccion fisica de comienzo del bootsector (copiado)
#define BOOTSECTOR 0x00001000
// direccion fisica de comienzo del kernel
#define KERNEL 0x00001200
// direccion fisica del buffer de video
#define VIDEO 0x000B8000

/* Direcciones virtuales de código, pila y datos */
/* -------------------------------------------------------------------------- */

// direccion de los codigos
#define TASK_CODE_VIRTUAL 0x01D00000
#define PHY_RICK_START    0x01D00000
#define PHY_MORTY_START   0x01D04000
#define RICK_CODE_START   0x10000
#define MORTY_CODE_START  0x14000
#define MEESEEKS_CODE_START 0x08000000
#define TASK_PAGES        4

/* Direcciones fisicas de codigos */
/* -------------------------------------------------------------------------- */
/* En estas direcciones estan los códigos de todas las tareas. De aqui se
 * copiaran al destino indicado por TASK_<X>_PHY_START.
 */

#define MAP_PHY_START 0x400000

/* Direcciones fisicas de directorios y tablas de paginas del KERNEL */
/* -------------------------------------------------------------------------- */
#define KERNEL_PAGE_DIR     0x00025000
#define KERNEL_PAGE_TABLE_0 0x00026000
#define KERNEL_STACK        0x00025000

#endif //  __DEFINES_H__
