/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================

  Definicion de la tabla de descriptores globales
*/

#include "gdt.h"
#include "tss.h"
#include "defines.h"




gdt_entry_t gdt[GDT_COUNT] = {
    /* Descriptor nulo*/
    /* Offset = 0x00 */
    [GDT_IDX_NULL] =
        {
            .limit_15_0 = 0x0000,
            .base_15_0 = 0x0000,
            .base_23_16 = 0x00,
            .type = 0x0,
            .s = 0x00,
            .dpl = 0x00,
            .p = 0x00,
            .limit_19_16 = 0x00,
            .avl = 0x0,
            .l = 0x0,
            .db = 0x0,
            .g = 0x00,
            .base_31_24 = 0x00,
        },
    [CS_RING_0] =
        {
            .base_15_0 = 0x0000,
            .base_23_16 = 0x00,
            .base_31_24 = 0x00,
            .limit_15_0 = 0xC8FF,
            .limit_19_16 = 0x00,
            .type = 0xA,                //execute/read
            .s = 0x01,                  //Si es codigo/datos va en uno
            .dpl = 0x00,
            .p = 0x01,                  //1 si el segmento esta en la memoria fisica
            .avl = 0x0,
            .l = 0x0,                   //1 si trabajamos en 64bits
            .db = 0x1,                  //1 si trabajamos en 32bits  
            .g = 0x01,                  //Granularidad, va en 1 si hay que direccionar mas de 1mb
        },

    [DS_RING_3] =
        {
            .base_15_0 = 0x0000,
            .base_23_16 = 0x00,
            .base_31_24 = 0x00,
            .limit_15_0 = 0xC8FF,
            .limit_19_16 = 0x00,
            .type = 0x2,                //read/write
            .s = 0x01,                  //Si es codigo/datos va en uno
            .dpl = 0x03,
            .p = 0x01,                  //1 si el segmento esta en la memoria fisica
            .avl = 0x0,
            .l = 0x0,                   //1 si trabajamos en 64bits
            .db = 0x1,                  //1 si trabajamos en 32bits  
            .g = 0x01,                  //Granularidad, va en 1 si hay que direccionar mas de 1mb
        },

    [DS_RING_0] =
        {
            .base_15_0      = 0x0000,
            .base_23_16     = 0x00,
            .base_31_24     = 0x00,
            .limit_15_0     = 0xc8ff,
            .limit_19_16    = 0x00,
            .type           = 0x2,
            .s              = 0x01,
            .dpl            = 0x00,
            .p              = 0x01,
            .avl            = 0x0,
            .l              = 0x0,
            .db             = 0x1,
            .g              = 0x01,

        },

    [CS_RING_3] =
        {
            .base_15_0      = 0x0000,
            .base_23_16     = 0x00,
            .base_31_24     = 0x00,
            .limit_15_0     = 0xc8ff,
            .limit_19_16    = 0x00,
            .type           = 0x0A,
            .s              = 0x01,
            .dpl            = 0x03,
            .p              = 0x01,
            .avl            = 0x0,
            .l              = 0x0,
            .db             = 0x1,
            .g              = 0x01,

        },

    [SCREEN_MEM_0] =
        {
            .base_15_0      = 0x8000,
            .base_23_16     = 0x0B,
            .base_31_24     = 0x00,
            .limit_15_0     = 0x1F3F,
            .limit_19_16    = 0x00,
            .type           = 0x02,
            .s              = 0x01,
            .dpl            = 0x00,
            .p              = 0x01,
            .avl            = 0x0,
            .l              = 0x0,
            .db             = 0x1,
            .g              = 0x00,

        },

    [INIT_TASK] =
        {
            .base_15_0      = 0x0000,
            .base_23_16     = 0x00,
            .base_31_24     = 0x00,                     
            .limit_15_0     = 0x67,
            .limit_19_16    = 0x00,
            .type           = 0x09,                     //Tipo=9 por ser tss
            .s              = 0x00,                     //S=0 porque este es de sistema
            .dpl            = 0x00,                     //DPL=0
            .p              = 0x01,                     //Presente = 1
            .avl            = 0x0,                      
            .l              = 0x0,                      //0 por manual
            .db             = 0x0,                      //0 por manual
            .g              = 0x00,                     //Es chiquito asi que g va en 0

        },

    [IDLE_TASK] =
        {
            .base_15_0      = 0x0000,
            .base_23_16     = 0x00,
            .base_31_24     = 0x00,                     
            .limit_15_0     = 0x67,
            .limit_19_16    = 0x00,
            .type           = 0x09,                     //Tipo=9 por ser tss
            .s              = 0x00,                     //S=0 porque este es de sistema
            .dpl            = 0x00,                     //DPL=0
            .p              = 0x01,                     //Presente = 1
            .avl            = 0x0,                      
            .l              = 0x0,                      //0 por manual
            .db             = 0x0,                      //0 por manual
            .g              = 0x00,                     //Es chiquito asi que g va en 0

        },


};

gdt_descriptor_t GDT_DESC = {sizeof(gdt) - 1, (uint32_t)&gdt};
