/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================

  Definicion de estructuras para administrar tareas
*/

#include "tss.h"
#include "defines.h"
#include "kassert.h"
#include "mmu.h"


tss_t tss_initial = {0};


tss_t tss_idle = {0};

tss_t tss_rick = {0};
tss_t tss_morty = {0};

tss_t tss_meeseeks[20] = {0};

void tss_inicial_init(void){
	gdt[INIT_TASK].base_15_0  = (uint32_t)(&tss_initial) & 0xffff;
	gdt[INIT_TASK].base_23_16 = ((uint32_t)(&tss_initial)>>16) & 0xff;
	gdt[INIT_TASK].base_31_24 = ((uint32_t)(&tss_initial)>>24) & 0xff;

}

void tss_idle_init(void){
	tss_idle.cr3 = 0x25000;			//Mismo cr3 que el kernel
  	tss_idle.eip = 0x18000;			//Aca empieza el codigo idle
  	tss_idle.eflags = 0x202;		//Habilito interrupciones
  	tss_idle.ebp = 0x25000;
  	tss_idle.esp = 0x25000;			//Misma pila que el kernel
	tss_idle.cs  = CS_RING_0_SEL;
	tss_idle.ss  = DS_RING_0_SEL;
	tss_idle.es  = DS_RING_0_SEL;
	tss_idle.ds  = DS_RING_0_SEL;
	tss_idle.fs  = DS_RING_0_SEL;
	tss_idle.gs  = DS_RING_0_SEL;
	tss_idle.iomap   = 0xffff;

	gdt[IDLE_TASK].base_15_0  = (uint32_t)(&tss_idle) & 0xffff;
	gdt[IDLE_TASK].base_23_16 = ((uint32_t)(&tss_idle)>>16) & 0xff;
	gdt[IDLE_TASK].base_31_24 = ((uint32_t)(&tss_idle)>>24) & 0xff;

}

uint32_t get_rick_cr3(){
	return tss_rick.cr3;
}

uint32_t get_morty_cr3(){
	return tss_morty.cr3;
}
uint32_t rick_init(){
	uint32_t task_cr3 = mmu_init_task_dir(0x1D00000, 0x10000, TASK_PAGES);
	uint32_t pila_nivel0 = mmu_next_free_kernel_page();

	tss_rick.cr3 = task_cr3;			//Mismo cr3 que el kernel
  	tss_rick.eip = TASK_CODE_VIRTUAL;			//Aca empieza el codigo idle
  	tss_rick.eflags = 0x202;		//Habilito interrupciones
  	tss_rick.ebp = TASK_CODE_VIRTUAL + TASK_PAGES * PAGE_SIZE;
  	tss_rick.esp = TASK_CODE_VIRTUAL + TASK_PAGES * PAGE_SIZE;			//Misma pila que el kernel
	tss_rick.cs  = CS_RING_3_SEL;
	tss_rick.ss  = DS_RING_3_SEL;
	tss_rick.es  = DS_RING_3_SEL;
	tss_rick.ds  = DS_RING_3_SEL;
	tss_rick.fs  = DS_RING_3_SEL;
	tss_rick.gs  = DS_RING_3_SEL;
	tss_rick.iomap   = 0xffff;

	tss_rick.ss0 = DS_RING_0_SEL;
	tss_rick.esp0 = pila_nivel0 + PAGE_SIZE;

	gdt[RICK_TASK].base_15_0  = (uint32_t)(&tss_rick) & 0xffff;
	gdt[RICK_TASK].base_23_16 = ((uint32_t)(&tss_rick)>>16) & 0xff;
	gdt[RICK_TASK].base_31_24 = ((uint32_t)(&tss_rick)>>24) & 0xff;
    gdt[RICK_TASK].limit_15_0     = 0x67;
    gdt[RICK_TASK].limit_19_16    = 0x00;
    gdt[RICK_TASK].type           = 0x09;                     //Tipo=9 por ser tss
    gdt[RICK_TASK].s              = 0x00;                     //S=0 porque este es de sistema
    gdt[RICK_TASK].dpl            = 0x00;                     //DPL=0
    gdt[RICK_TASK].p              = 0x01;                     //Presente = 1
    gdt[RICK_TASK].avl            = 0x0;                      
    gdt[RICK_TASK].l              = 0x0;                      //0 por manual
    gdt[RICK_TASK].db             = 0x0;                      //0 por manual
    gdt[RICK_TASK].g              = 0x00;                     //Es chiquito asi que g va en 0

    return (uint32_t) task_cr3;
}

uint32_t morty_init(){
	uint32_t task_cr3 = mmu_init_task_dir(0x1D04000, 0x14000, TASK_PAGES);
	uint32_t pila_nivel0 = mmu_next_free_kernel_page();

	tss_morty.cr3 = task_cr3;									//Mismo cr3 que el kernel
  	tss_morty.eip = TASK_CODE_VIRTUAL;							//Aca empieza el codigo idle
  	tss_morty.eflags = 0x202;									//Habilito interrupciones
  	tss_morty.ebp = TASK_CODE_VIRTUAL + TASK_PAGES * PAGE_SIZE;
  	tss_morty.esp = TASK_CODE_VIRTUAL + TASK_PAGES * PAGE_SIZE;	//Misma pila que el kernel
	tss_morty.cs  = CS_RING_3_SEL;
	tss_morty.ss  = DS_RING_3_SEL;
	tss_morty.es  = DS_RING_3_SEL;
	tss_morty.ds  = DS_RING_3_SEL;
	tss_morty.fs  = DS_RING_3_SEL;
	tss_morty.gs  = DS_RING_3_SEL;
	tss_morty.iomap   = 0xffff;

	

	tss_morty.ss0 = DS_RING_0_SEL;
	tss_morty.esp0 = pila_nivel0 + PAGE_SIZE;

	gdt[MORTY_TASK].base_15_0  = (uint32_t)(&tss_morty) & 0xffff;
	gdt[MORTY_TASK].base_23_16 = ((uint32_t)(&tss_morty)>>16) & 0xff;
	gdt[MORTY_TASK].base_31_24 = ((uint32_t)(&tss_morty)>>24) & 0xff;
    gdt[MORTY_TASK].limit_15_0     = 0x67;
    gdt[MORTY_TASK].limit_19_16    = 0x00;
    gdt[MORTY_TASK].type           = 0x09;                     //Tipo=9 por ser tss
    gdt[MORTY_TASK].s              = 0x00;                     //S=0 porque este es de sistema
    gdt[MORTY_TASK].dpl            = 0x00;                     //DPL=0
    gdt[MORTY_TASK].p              = 0x01;                     //Presente = 1
    gdt[MORTY_TASK].avl            = 0x0;                      
    gdt[MORTY_TASK].l              = 0x0;                      //0 por manual
    gdt[MORTY_TASK].db             = 0x0;                      //0 por manual
    gdt[MORTY_TASK].g              = 0x00;                     //Es chiquito asi que g va en 0

    return (uint32_t) task_cr3;

}

void meeseeks_init(uint32_t pd_rick, uint32_t pd_morty){

	uint32_t pila_nivel0;

	for (uint32_t i = TASK_1; i <= TASK_20; ++i)
	{
		pila_nivel0 = mmu_next_free_kernel_page();

		gdt[i].base_15_0      = (uint32_t)(&tss_meeseeks[i-TASK_1]) & 0xffff;
        gdt[i].base_23_16     = ((uint32_t)(&tss_meeseeks[i-TASK_1])>>16) & 0xff;
        gdt[i].base_31_24     = ((uint32_t)(&tss_meeseeks[i-TASK_1])>>24) & 0xff;
        gdt[i].limit_15_0     = 0x67;
        gdt[i].limit_19_16    = 0x00;
        gdt[i].type           = 0x09;                     //Tipo=9 por ser tss
        gdt[i].s              = 0x00;                     //S=0 porque este es de sistema
        gdt[i].dpl            = 0x00;                     //DPL=0
        gdt[i].p              = 0x01;                     //Presente = 1
        gdt[i].avl            = 0x0;                      
        gdt[i].l              = 0x0;                      //0 por manual
        gdt[i].db             = 0x0;                      //0 por manual
        gdt[i].g              = 0x00;                     //Es chiquito asi que g va en 0


    if(i <= TASK_10){
    	tss_meeseeks[i-TASK_1].cr3 = pd_rick;			//Se copia del cr3 del jugador que lo llame
    	tss_meeseeks[i-TASK_1].eip = MEESEEKS_CODE_START + PAGE_SIZE * 2 * (i-TASK_1);				
    }else{
    	tss_meeseeks[i-TASK_1].cr3 = pd_morty;
    	tss_meeseeks[i-TASK_1].eip = MEESEEKS_CODE_START + PAGE_SIZE * 2 * (i-TASK_11);
    }
  				
	tss_meeseeks[i-TASK_1].ebp = tss_meeseeks[i-TASK_1].eip + PAGE_SIZE * 2;	
	tss_meeseeks[i-TASK_1].esp = tss_meeseeks[i-TASK_1].eip + PAGE_SIZE * 2;		
  	tss_meeseeks[i-TASK_1].eflags = 0x202;									//Habilito interrupciones
	tss_meeseeks[i-TASK_1].cs  = CS_RING_3_SEL;
	tss_meeseeks[i-TASK_1].ss  = DS_RING_3_SEL;
	tss_meeseeks[i-TASK_1].es  = DS_RING_3_SEL;
	tss_meeseeks[i-TASK_1].ds  = DS_RING_3_SEL;
	tss_meeseeks[i-TASK_1].fs  = DS_RING_3_SEL;
	tss_meeseeks[i-TASK_1].gs  = DS_RING_3_SEL;
	tss_meeseeks[i-TASK_1].iomap   = 0xffff;



	tss_meeseeks[i-TASK_1].ss0 = DS_RING_0_SEL;
	tss_meeseeks[i-TASK_1].esp0 = pila_nivel0 + PAGE_SIZE;
	}
}

void tss_init() {
	uint32_t pd_rick  = rick_init();
	uint32_t pd_morty = morty_init();
	meeseeks_init(pd_rick, pd_morty);
	tss_inicial_init();
	tss_idle_init();
}

uint32_t reiniciar_tss(uint8_t meeseeks_tss_index){

	uint32_t new_virtual = MEESEEKS_CODE_START + MEESEEKS_SIZE * (meeseeks_tss_index%10);
	
	tss_meeseeks[meeseeks_tss_index].eip = new_virtual;
	tss_meeseeks[meeseeks_tss_index].esp = new_virtual + MEESEEKS_SIZE;
	tss_meeseeks[meeseeks_tss_index].ebp = new_virtual + MEESEEKS_SIZE;

	
	tss_meeseeks[meeseeks_tss_index].eflags = 0x202;
	tss_meeseeks[meeseeks_tss_index].cs  = CS_RING_3_SEL;
	tss_meeseeks[meeseeks_tss_index].ss  = DS_RING_3_SEL;
	tss_meeseeks[meeseeks_tss_index].es  = DS_RING_3_SEL;
	tss_meeseeks[meeseeks_tss_index].ds  = DS_RING_3_SEL;
	tss_meeseeks[meeseeks_tss_index].fs  = DS_RING_3_SEL;
	tss_meeseeks[meeseeks_tss_index].gs  = DS_RING_3_SEL;
	tss_meeseeks[meeseeks_tss_index].iomap   = 0xffff;

	return new_virtual;
}

