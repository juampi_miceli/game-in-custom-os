; ** por compatibilidad se omiten tildes **
; ==============================================================================
; TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
; ==============================================================================
;
; Definicion de rutinas de atencion de interrupciones

%include "print.mac"

BITS 32

sched_task_offset:     dd 0xFFFFFFFF
sched_task_selector:   dw 0xFFFF

;; Debug
debug_mode: db 0x00
last_task_selector: dw 0x00
debug_interrupt: db 0x00
showing_debug: db 0x00

%define y_key 0x15
%define ON 0x01
%define OFF 0x00

extern print_int

;; PIC
extern pic_finish1

extern printPressedKey
extern clearReleasedKey

;; Sched
extern sched_next_task
extern desalojar_tarea
extern alojar_tarea
extern reduce_movement
extern imprimir_relojes

%define IDLE_TASK_SEL   0x0080

%define RICK_TASK_SEL   0x88       
%define MORTY_TASK_SEL  0x90    

;; Game
extern print_scores
extern move_meeseeks
extern meeseeks_look
extern portal_gun
extern check_winner
extern imprimir_debugger
extern actualizar_pantalla

;;
;; Definición de MACROS
;; -------------------------------------------------------------------------- ;;
%macro ISR 1
global _isr%1

_isr%1:
	; xchg bx, bx
    cmp byte [debug_mode], OFF
    jz .desalojar

    mov byte [showing_debug], ON

    pushad
    str ax
    push eax
    mov eax, cr0
    push eax
    mov eax, cr2
    push eax
    mov eax, cr3
    push eax
    mov eax, cr4
    push eax
    push %1
    push ds
    push es
    push fs
    push gs
    push esp
    call imprimir_debugger
    pop esp
    pop gs
    pop fs
    pop es
    pop ds
    add esp, 4*6
    popad

    ; xchg bx, bx
    
  .desalojar:
    str bx                                  ;bx = selector de la tarea llamadora
    push ebx
    call desalojar_tarea
    add esp, 4

    cmp byte [showing_debug], ON
    jz .idle_jump
    call actualizar_pantalla
    jmp .idle_jump

    
  .idle_jump:
    ; xchg bx, bx
    jmp IDLE_TASK_SEL:0x0

    jmp $                                    ;Esto no se ejecuta nunca


%endmacro


;; Rutina de atención de las EXCEPCIONES
;; -------------------------------------------------------------------------- ;;
ISR 0
ISR 1
ISR 2
ISR 3
ISR 4
ISR 5
ISR 6
ISR 7
ISR 8
ISR 9
ISR 10
ISR 11
ISR 12
ISR 13
ISR 14
ISR 16
ISR 17
ISR 18
ISR 19


;; Rutina de atención del RELOJ
;; -------------------------------------------------------------------------- ;;
global _isr32 

_isr32: 
	pushad 
    
    call pic_finish1
    cmp byte [showing_debug], ON
    jz .fin

    ; xchg bx, bx
    ; call actualizar_pantalla
	call next_clock
    ;Si no hay mas semillas
    ;Si es desalojada una tarea R/M
    call print_scores

    call check_winner

    call sched_next_task

    push ax
    call reduce_movement                ;Solo si no es rick o morty
    pop ax

    str bx
    cmp ax, bx
    jz  .fin
    cmp word ax, -1
    jz  .fin

    ; xchg bx, bx
    mov word [sched_task_selector], ax
    jmp far [sched_task_offset]

.fin:
	popad 
	iret

;; Rutina de atención del TECLADO
;; -------------------------------------------------------------------------- ;;
global _isr33

_isr33:
	pushad

    call pic_finish1
    
	in al, 0x60
    ; xchg bx, bx
    cmp al, y_key
    jz .toggle_debug
    jmp .end


  .toggle_debug:
    ; xchg bx, bx
    cmp  byte [debug_mode], OFF
    jz .turn_on
    jmp .turn_off

  .turn_on:
    mov byte [debug_mode], ON
    jmp .end

  .turn_off:
    cmp byte [showing_debug], ON
    jz .reanudar_ejecucion
    mov byte [debug_mode], OFF
    jmp .end



  .reanudar_ejecucion:
    mov byte [showing_debug], OFF
    call actualizar_pantalla
    jmp .end

    

  .end:
	popad
	iret

;; Rutinas de atención de las SYSCALLS
;; -------------------------------------------------------------------------- ;;

global _isr88
global _isr89
global _isr100
global _isr123

;MEESEEKS
;Llega la dir del codigo, el x, el y
;eax = code_dir
;ebx = X
;ecx = Y

%define VIRTUAL_START 	0x1D00000
%define VIRTUAL_END 	0x1D03FFE
_isr88:
	; xchg bx, bx
    push ebp
    mov ebp, esp
    sub esp, 4 
    pushad



    cmp eax, VIRTUAL_START
    jl  .error_fatal
    cmp eax, VIRTUAL_END
    jg  .error_fatal	
    jmp .dir_valida


  .error_fatal:
  	str si
  	jmp .desalojar


  .dir_valida:
    cmp ebx, 0
    jl  .error_fatal
    cmp ebx, 79
    jg  .error_fatal
    cmp ecx, 0
    jl  .error_fatal
    cmp ecx, 39
    jg  .error_fatal


    str si
    cmp si, RICK_TASK_SEL
    jz  .alojar
    cmp si, MORTY_TASK_SEL
    jz  .alojar
    jmp .desalojar   
    
  .alojar:   
    push ecx
    push ebx
    push eax
    
    call alojar_tarea
    mov dword [ebp-4], eax
    
    add esp, 4*3
    jmp .fin_de_turno


  .desalojar:
    push esi
    call desalojar_tarea
    add esp, 4
    jmp .fin_de_turno


  .fin_de_turno:
  	call actualizar_pantalla
    jmp IDLE_TASK_SEL:0x0

    popad

    mov eax, [ebp-4]
    add esp, 4
    pop ebp
    iret


;PORTAL GUN
_isr89:
    pushad


    str bx                                  ;bx = selector de la tarea llamadora
    cmp bx, RICK_TASK_SEL
    jz  .desalojar
    cmp bx, MORTY_TASK_SEL
    jz  .desalojar
    jmp .portal_start


  .desalojar:
    push ebx
    call desalojar_tarea
    add esp, 4
    jmp .idle_jump


  .portal_start:

    call portal_gun

  .idle_jump:
  	call actualizar_pantalla
    jmp IDLE_TASK_SEL:0x0
    
    popad
    iret


;LOOK
;return eax = [dx]
;return ebx = [dy]
_isr100:
    ; xchg bx, bx
    push ebp
    mov ebp, esp
    sub esp, 8
    pushad

    str si                                  ;si = selector de la tarea llamadora
    cmp si, RICK_TASK_SEL
    jz  .invalid
    cmp si, MORTY_TASK_SEL
    jz  .invalid
    jmp .look_start

  .look_start:
    mov ebx, ebp
    sub ebx, 8
    mov eax, ebp
    sub eax, 4
    push ebx
    push eax
    call meeseeks_look
    add esp, 4*2
    jmp .idle_jump

  .invalid:
    mov byte [ebp-4], -1
    mov byte [ebp-8], -1
    jmp .idle_jump
    

  .idle_jump:
    jmp IDLE_TASK_SEL:0x0
    
    popad

    mov al, [ebp-4]
    mov bl, [ebp-8]
    add esp, 8
    pop ebp
    iret


;MOVE
;eax = desplazamiento horizontal
;ebx = desplazamiento vertical
_isr123:
	; xchg bx, bx
    push ebp
    mov ebp, esp
    sub esp, 4
    pushad
    
    str si                                  ;si = selector de la tarea llamadora
    cmp si, RICK_TASK_SEL
    jz  .desalojar
    cmp si, MORTY_TASK_SEL
    jz  .desalojar
    jmp .move_start


  .desalojar:
    push esi
    call desalojar_tarea
    add esp, 4
    jmp .idle_jump


  .move_start:
    str si
    push esi
    push ebx
    push eax
    call move_meeseeks
    mov byte [ebp-4], al
    add esp, 4*3


  .idle_jump:
  	call actualizar_pantalla
    jmp IDLE_TASK_SEL:0x0
    
    popad
    mov byte al, [ebp-4]
    add esp, 4
    pop ebp
    iret

;; Funciones Auxiliares
;; -------------------------------------------------------------------------- ;;
isrNumber:           dd 0x00000000
isrClock:            db '|/-\'
next_clock:
        pushad
        ; xchg bx, bx
        inc DWORD [isrNumber]
        mov ebx, [isrNumber]
        cmp ebx, 0x4
        jl .ok
                mov DWORD [isrNumber], 0x0
                mov ebx, 0
        .ok:
                mov eax, ebx
                add ebx, isrClock

                push eax
                call imprimir_relojes
                add esp, 4
                print_text_pm ebx, 1, 0x0f, 49, 79
                popad
        ret