#include "stddef.h"
#include "syscall.h"

uint64_t seed = 0xb0ffcec24519103dull;  //0xb0ffcec65319103dull


void meeseks1_func(void);
void meeseks2_func(void);
void meeseeks_buscador(void);


uint8_t abs(int8_t x);
void abort(void);
void save_confirmed_seed(int32_t pos);
uint32_t get_x_coord(int32_t pos);
uint32_t get_y_coord(int32_t pos);
uint32_t get_pos(int32_t x, int32_t y);
int32_t get_next_pos(void);
void remove_seed(int32_t pos);

uint32_t rand(void);



int32_t confirmed_seeds[5];
uint32_t res_meeseeks[5];
uint32_t meeseeks_pos[2];



void task(void) {
  int32_t pos;
  for (int i = 0; i < 5; ++i)
  {
    res_meeseeks[i] = 1;
    confirmed_seeds[i] = -1;
  }

  

  while(1){
    //Primero
    for(int i = 0; i < 2; i++){
      meeseeks_pos[i] = rand();
      meeseeks_pos[i] %= 80*40;
    }
    res_meeseeks[0] = syscall_meeseeks((uint32_t)&meeseks1_func, get_x_coord(meeseeks_pos[0]), get_y_coord(meeseeks_pos[0]));
    //Segundo
    res_meeseeks[1] = syscall_meeseeks((uint32_t)&meeseks2_func, get_x_coord(meeseeks_pos[1]), get_y_coord(meeseeks_pos[1]));
    if(res_meeseeks[0] != 0){
      //El meeseeks 0 fue agregado
      pos = get_next_pos();
      uint32_t not_found = syscall_meeseeks((uint32_t)&meeseeks_buscador, get_x_coord(pos), get_y_coord(pos));
      if(not_found == 0){
        remove_seed(pos);
      }
    }

    if(res_meeseeks[1] != 0){
      //El meeseeks 0 fue agregado
      pos = get_next_pos();

      uint32_t not_found = syscall_meeseeks((uint32_t)&meeseeks_buscador, get_x_coord(pos), get_y_coord(pos));
      if(not_found == 0){
        remove_seed(pos);
      }
    }
  }
}

void meeseks1_func(void) {

  int8_t x = -1;
  int8_t y = -1;
  syscall_look(&x, &y);

  if(abs(x) + abs(y) <= 7){
    syscall_move(x, y);
  }else{
    save_confirmed_seed(get_pos(x+get_x_coord(meeseeks_pos[0]), y+get_y_coord(meeseeks_pos[0])));
  }
  abort();
}

void meeseks2_func(void) {
  int8_t x = -1;
  int8_t y = -1;
  syscall_look(&x, &y);

  if(abs(x) + abs(y) <= 7){
    syscall_move(x, y);
  }else{
    save_confirmed_seed(get_pos(x+get_x_coord(meeseeks_pos[1]), y+get_y_coord(meeseeks_pos[1])));
  }
  abort();
}


void meeseeks_buscador(){
  //Si entro es porque meti mal la pos
  abort();
}




uint8_t abs(int8_t x){
  if(x<0){
    return -x;
  }else{
    return x;
  }

  return 0;
}

void save_confirmed_seed(int32_t pos){
  for (int i = 0; i < 5; ++i)
  {
    if(pos == confirmed_seeds[i]){
      return;
    }
  }
  int i = 0;
  while(i < 5 && confirmed_seeds[i] != -1){
    i++;
  }
  if(i<5){
    //Habia lugar
    confirmed_seeds[i] = pos;
  }else{
    //No habia lugar, reemplazo alguna semilla que puede haber desaparecido
    uint32_t r_id = rand();
    r_id %= 5;
    confirmed_seeds[r_id] = pos;
  }
}

uint32_t get_pos(int32_t x, int32_t y){
  return 80*y+x;
}

uint32_t get_x_coord(int32_t pos){
  return pos%80;
}

uint32_t get_y_coord(int32_t pos){
  return pos/80;
}

int32_t get_next_pos(){
  int i = 0;

  while(i < 5 && confirmed_seeds[i] == -1){
    i++;
  }

  if(i<5){
    return confirmed_seeds[i];
  }

  uint32_t res = rand();
  res %= 80*40;

  return res;
}


void remove_seed(int32_t pos){
  int i = 0;

  while(i < 5 && (confirmed_seeds[i] != pos)){
    i++;
  }

  if(i<5){
    confirmed_seeds[i] = -1;
  }
}

uint32_t rand(void) {
  seed = 6364136223846793005ULL * seed + 1;
  return seed;
}

void abort(){
  uint8_t *p = 0;
  p[0] = 42;
}

